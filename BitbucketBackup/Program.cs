﻿using System;
using System.IO;
using System.Linq;
using Brevity;

namespace BitbucketBackup
{
    internal class Program
    {
        internal static readonly string WorkingDirectory = $"./repositories";

        private static void Main(string[] args)
        {
            //parse and get options
            var options = Options.GetOptions(args);

            //output the options
            ConsoleWrite(options.ToString());

            //fetch metadata from bitbucket
            var result = Repo.GetRepositoriesResponse(options);

            //if no response...
            if (result == null)
            {
                Console.Error.WriteLine("Something went wrong....");
                return;
            }

            //loop repositories
            ConsoleWrite("Cloning...");
            var total = result.Count;
            var i = 0;

            var list = result.OrderBy(a => a.Owner + "/" + a.Slug).ToList();
            list.AsParallel().ForAll(repository =>
            //foreach (var repository in list)
            {
                try
                {
                    var x = ++i;

                    var index = list.IndexOf(repository);

                    if (repository.Slug.StartsWith("svn_"))
                    {
                        Console.WriteLine("** Skipping....");
                        return;
                    }

                    var gitZip = $"{options.Directory}{repository.Owner}-{repository.Slug}-git.zip";
                    var wikiZip = $"{options.Directory}{repository.Owner}-{repository.Slug}-wiki.zip";

                    ConsoleWrite($"({x}) {index}/{total} - {repository.Owner}/{repository.Slug}");

                    if (File.Exists(gitZip))
                    {
                        ConsoleWrite($"({x}) {index}/{total} - Git export already exists. Skipping...");
                    }
                    else
                    {
                        var dir = "{0}/{1}-{2}".FormatWith(WorkingDirectory, repository.Owner, repository.Slug);
                        try
                        {
                            if (Directory.Exists(dir))
                            {
                                Git.Pull(options, dir);
                            }
                            else
                            {
                                Git.Clone(options, repository, "", dir);
                            }
                            if (Directory.Exists(dir) && options.Zip)
                            {
                                //zip result
                                ConsoleWrite($"({x}) {index}/{total} - {repository.Owner}/{repository.Slug} - Zipping... {gitZip}");
                                Zipper.Zip(dir, gitZip);
                                Cleanup.Delete(dir);
                            }
                        }
                        catch (Exception ex)
                        {
                            ConsoleWrite($"({x}) {index}/{total} - {repository.Owner}/{repository.Slug} - Error:{ex.Message}");
                        }
                    }

                    if (repository.Has_Wiki)
                    {
                        if (File.Exists(wikiZip))
                        {
                            ConsoleWrite($"({x}) {index}/{total} - Wiki export already exists. Skipping...");
                        }
                        else
                        {
                            var wikiDir = "{0}/{1}_wiki".FormatWith(WorkingDirectory, repository.Slug);
                            //ConsoleWrite(clone);
                            if (Directory.Exists(wikiDir))
                            {
                                Git.Pull(options, wikiDir);
                            }
                            else
                            {
                                Git.Clone(options, repository, "/wiki", wikiDir);
                            }
                            if (Directory.Exists(wikiDir) && options.Zip)
                            {
                                Zipper.Zip(wikiDir, wikiZip);
                                Cleanup.Delete(wikiDir);
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    ConsoleWrite($"!! Error in {repository.Owner}/{repository.Slug}: {ex.Message}");
                }
            }
            );

            //finished
            ConsoleWrite("Finished");
        }

        private static void ConsoleWrite(string s)
        {
            var c = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(s);
            Console.ForegroundColor = c;
        }
    }
}