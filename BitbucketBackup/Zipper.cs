﻿using System;
using System.IO;
using Brevity;
using Ionic.Zip;
using Ionic.Zlib;

namespace BitbucketBackup
{
    public class Zipper
    {
        public static void Zip(string directory, string filename)
        {
            Console.WriteLine("--- " + directory);
            if (!Directory.Exists(directory))
            {
                Console.WriteLine(directory + " does not exist");
                return;
            }

            using (var zip = new ZipFile()
            {
                //CompressionLevel = CompressionLevel.BestCompression,
                //StatusMessageTextWriter = Console.Out,
                ZipErrorAction = ZipErrorAction.InvokeErrorEvent,
            })
            {
                zip.ZipError += (sender, args) =>
                {
                    Console.WriteLine("Error in zipping: " + args.Exception.Message);
                };

                zip.AddDirectory(directory);
                if (!Directory.Exists(Path.GetDirectoryName(filename)))
                {
                    Console.Write("------" + filename);
                    Directory.CreateDirectory(Path.GetDirectoryName(filename));
                }

                //foreach (var dir in Directory.GetDirectories(directory))
                //{
                //	zip.AddDirectory(dir);
                //}
                //zip.AddFiles(Directory.GetFiles(directory));
                zip.Save(filename);
            }
        }
    }
}