@ECHO OFF 
ECHO ***************
ECHO * NUGET RESTORE
call nuget restore
ECHO ***************
ECHO * BUILD
call msbuild /property:Configuration=Release
ECHO ***************
