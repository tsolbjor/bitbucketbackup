﻿using System;
using System.Diagnostics;
using Brevity;

namespace BitbucketBackup
{
    public class Git
    {
     
        
        private static void Go(string app, string param, string workingDirectory)
        {
            var start = new ProcessStartInfo(app, param)
            {
                RedirectStandardError = false,
                RedirectStandardOutput = false,
                RedirectStandardInput = false,
                UseShellExecute = false,
                
                
            };
            if(workingDirectory != null)
            {
                start.WorkingDirectory = workingDirectory;
            }

            using (Process p = Process.Start(start))
            {
                p.WaitForExit();
            }
        }

        internal static void Clone(Options options, Repo.Repo2 repository, string v, string dir)
        {
            string clone = $"https://{options.Username}:{options.Password}@bitbucket.org/{repository.Owner}/{repository.Slug}.git" + v;
            string remote = $"https://{options.Username}@bitbucket.org/{repository.Owner}/{repository.Slug}.git" + v;
            Go("git", "clone {2} {0} {1}".FormatWith(clone, dir, options.CloneDepth > 0 ? "--depth " + options.CloneDepth : null), null);
            Go("git", $"remote set-url origin {remote}", dir);
        }

        public static void Pull(Options options, string dir)
        {
            Go("git", "pull", dir);
        }
    }
}