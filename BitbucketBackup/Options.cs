﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using Brevity;
using CommandLine;

namespace BitbucketBackup
{
    public class Options
    {
        private static readonly string DefaultFilename = DateTime.Now.ToString("yyyy-MM-dd_hh-mm-ss") + ".zip";
        private string _directory;
        private string _filename;

        [Option('u', "username", Required = true)]
        public string Username { get; set; }

        [Option('p', "password", Required = false)]
        public string Password { get; set; }

        [Option('t', "team", Required = false)]
        public string Team { get; set; }

        [Option('d', "directory", Required = false, HelpText = "Defaults to current directory")]
        public string Directory
        {
            get { return _directory ?? ".\\"; }
            set { _directory = value; }
        }

        [Option('f', "filename", Required = false, HelpText = "Defaults to DateTime.Now")]
        public string Filename
        {
            get { return _filename ?? DefaultFilename; }
            set { _filename = value; }
        }

        [Option('v', "verbose")]
        public bool Verbose { get; set; }

        [Option('z', "zip", HelpText = "If specified, repository is zipped")]
        public bool Zip { get; set; }

        [Option("CloneDepth", DefaultValue = 0)]
        public int CloneDepth { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("OPTIONS");
            sb.AppendLine("-u {0}".FormatWith(Username));
            sb.AppendLine("-p {0}".FormatWith(Password.Mask(0)));
            sb.AppendLine("-t {0}".FormatWith(Team));
            sb.AppendLine("-d {0}".FormatWith(Directory));
            sb.AppendLine("-f {0}".FormatWith(Filename));
            sb.AppendLine("-v {0}".FormatWith(Verbose));
            return sb.ToString();
        }

        public void EnsurePassword()
        {
            if (Password != null) return;
            Console.Write("Password: ");
            Password = ReadPassword();
        }

        private string ReadPassword()
        {
            string pass = "";
            ConsoleKeyInfo key;
            while ((key = Console.ReadKey(true)).Key != ConsoleKey.Enter)
            {
                // Backspace Should Not Work

                if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                {
                    pass += key.KeyChar;
                    Console.Write("*");
                }
            }
            Console.WriteLine();
            return pass;
        }

        public static Options GetOptions(string[] args)
        {
            var options = new Options();
            Parser.Default.ParseArgumentsStrict(args, options);
            options.EnsurePassword();
            return options;
        }
    }
}