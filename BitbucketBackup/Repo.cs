﻿using System;
using System.Collections.Generic;
using System.Linq;
using Brevity;
using RestSharp;

namespace BitbucketBackup
{
    public class Repo
    {
        public static List<Repo2> GetRepositoriesResponse(Options options)
        {
            var client = new RestClient("https://api.bitbucket.org/1.0/")
            {
                Authenticator = new HttpBasicAuthenticator(options.Username, options.Password)
            };
            var request = new RestRequest("user/repositories", Method.GET);
            IRestResponse response = client.Execute(request);

            if (options.Verbose)
            {
                //Console.Write(response.Content);
                Console.WriteLine(response.Content.FromJson<List<Repo2>>().Select(a => a.Slug).ToJson());
            }
            var result = response.Content.FromJson<List<Repo2>>() ?? new List<Repo2>();
            //result.StatusDescription = response.StatusDescription;

            return result;
        }


        public class RepositoriesResponse
        {
            public string StatusDescription { get; set; }

            public IEnumerable<Repository> Values { get; set; }

            public string Next { get; set; }
        }

        public class Repo2
        {
            public bool Has_Wiki { get; set; }
            public string Owner { get; set; }
            public string Slug { get; set; }

        }

        public class Repository
        {
            public string Name { get; set; }
            public bool Has_Wiki { get; set; }
            public LinksObj Links { get; set; }

            public class LinksObj
            {
                public IEnumerable<CloneObj> Clone { get; set; }

                public class CloneObj
                {
                    public string Href { get; set; }
                    public string Name { get; set; }
                }
            }
        }
    }
}