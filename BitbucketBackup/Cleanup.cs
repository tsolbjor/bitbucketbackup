﻿using System.IO;

namespace BitbucketBackup
{
    public class Cleanup
    {
        public static void Delete(string path)
        {
            foreach (string file in Directory.GetFiles(path))
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            foreach (string dir in Directory.GetDirectories(path))
            {
                Delete(dir);
            }

            Directory.Delete(path);
        }
    }
}